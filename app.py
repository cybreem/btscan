#!/usr/bin/python
from threading import Lock
from flask import Flask, render_template, session, request, \
    copy_current_request_context
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect
from flask import jsonify
# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
async_mode = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None
thread_lock = Lock()


def background_thread():
    """Example of how to send server generated events to clients."""
    count = 0
    while True:
        socketio.sleep(10)
        count += 1
        socketio.emit('my_response',
                      {'data': 'Server generated event', 'count': count},
                      namespace='/test')


@app.route('/')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)

@app.route('/monitor')
def monitor():
    table_list = {
        1:{'table_name': 'Table 1', 'code': 'table-1'},
        2:{'table_name': 'Table 2', 'code': 'table-2'},
        3:{'table_name': 'Table 3', 'code': 'table-3'},
        4:{'table_name': 'Table 4', 'code': 'table-4'},
        5:{'table_name': 'Table 5', 'code': 'table-5'}
    }
    return render_template('monitor.html', table_list=table_list,async_mode=socketio.async_mode)


@socketio.on('my_event', namespace='/test')
def test_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': message['data'], 'count': session['receive_count']})


@socketio.on('my_broadcast_event', namespace='/test')
def test_broadcast_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': message['data'], 'count': session['receive_count']},
         broadcast=True)


@socketio.on('join', namespace='/test')
def join(message):
    join_room(message['room'])
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': 'In rooms: ' + ', '.join(rooms()),
          'count': session['receive_count']})


@socketio.on('leave', namespace='/test')
def leave(message):
    leave_room(message['room'])
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': 'In rooms: ' + ', '.join(rooms()),
          'count': session['receive_count']})


@socketio.on('close_room', namespace='/test')
def close(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response', {'data': 'Room ' + message['room'] + ' is closing.',
                         'count': session['receive_count']},
         room=message['room'])
    close_room(message['room'])


@socketio.on('my_room_event', namespace='/test')
def send_room_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': message['data'], 'count': session['receive_count']},
         room=message['room'])


@socketio.on('disconnect_request', namespace='/test')
def disconnect_request():
    @copy_current_request_context
    def can_disconnect():
        disconnect()

    session['receive_count'] = session.get('receive_count', 0) + 1
    # for this emit we use a callback function
    # when the callback function is invoked we know that the message has been
    # received and it is safe to disconnect
    emit('my_response',
         {'data': 'Disconnected!', 'count': session['receive_count']},
         callback=can_disconnect)


@socketio.on('my_ping', namespace='/test')
def ping_pong():
    emit('my_pong')


@socketio.on('connect', namespace='/test')
def test_connect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(background_thread)
    emit('my_response', {'data': 'Connected', 'count': 0})


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected', request.sid)



@app.route('/btpush/<table>', methods = ['POST'])
def btpush(table):
    data = request.form
    addr = data.get('addr')
    name = data.get('name')
    signal = data.get('signal')
    cust_list = {
      'mcaddr1': {'name': 'Alexandro Putra', 'email':'alexandro.putra@gmail.com'},
      'mcaddr2': {'name': 'Muarif Gustiar', 'email':'muarif@gmail.com'},
      'mcaddr3': {'name': 'Fadli Maulana', 'email':'fadli@gmail.com'},
      'mcaddr4': {'name': 'Glentino Putra', 'email':'glenn.putra@gmail.com'},
      'mcaddr5': {'name': 'Essa Faizal', 'email':'exsza@gmail.com'},
      'mcaddr6': {'name': 'Abdullah Kariim', 'email':'cybreem@gmail.com'},
      'mcaddr7': {'name': 'Adde Namnung', 'email':'ade.namnung@gmail.com'}
    }
    session['receive_count'] = session.get('receive_count', 0) + 1
    response = {'table': table, 'customer': cust_list[addr]['name'] if (addr in cust_list) else 'Guest '+name, 'addr': addr, 'signal': signal}
    emit('my_response',
         {'data': 'new bluetooth detected', 'response': response, 'count': session['receive_count']},
         broadcast=True, namespace='/test')
    return {'result': response}, 200


if __name__ == '__main__':
    socketio.run(app, debug=True, host='0.0.0.0', port='8900')
